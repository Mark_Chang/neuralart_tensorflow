import tensorflow as tf
import numpy as np
import scipy.io
import scipy.misc
import os


IMAGE_W = 400 
IMAGE_H = 300 
MEAN_VALUES = np.array([104, 117, 123]).reshape((1,1,1,3))

def build_net(ntype, nin, nwb=None):
  if ntype == 'conv':
    return tf.nn.relu(tf.nn.conv2d(nin, nwb[0], strides=[1, 1, 1, 1], padding='SAME')+ nwb[1])
  elif ntype == 'pool':
    return tf.nn.avg_pool(nin, ksize=[1, 2, 2, 1],
                  strides=[1, 2, 2, 1], padding='SAME')

def get_weight_bias(vgg_layers, i,):
  weights = vgg_layers[i][0][0][0][0][0]
  weights = tf.constant(weights)
  bias = vgg_layers[i][0][0][0][0][1]
  bias = tf.constant(np.reshape(bias, (bias.size)))
  return weights, bias

def build_vgg19(path):
  net = {}
  vgg_rawnet = scipy.io.loadmat(path)
  vgg_layers = vgg_rawnet['layers'][0]
  net['input'] = tf.Variable(np.zeros((1, IMAGE_H, IMAGE_W, 3)).astype('float32'))
  net['conv1_1'] = build_net('conv',net['input'],get_weight_bias(vgg_layers,0))
  net['conv1_2'] = build_net('conv',net['conv1_1'],get_weight_bias(vgg_layers,2))
  net['pool1']   = build_net('pool',net['conv1_2'])
  net['conv2_1'] = build_net('conv',net['pool1'],get_weight_bias(vgg_layers,5))
  net['conv2_2'] = build_net('conv',net['conv2_1'],get_weight_bias(vgg_layers,7))
  net['pool2']   = build_net('pool',net['conv2_2'])
  net['conv3_1'] = build_net('conv',net['pool2'],get_weight_bias(vgg_layers,10))
  net['conv3_2'] = build_net('conv',net['conv3_1'],get_weight_bias(vgg_layers,12))
  net['conv3_3'] = build_net('conv',net['conv3_2'],get_weight_bias(vgg_layers,14))
  net['conv3_4'] = build_net('conv',net['conv3_3'],get_weight_bias(vgg_layers,16))
  net['pool3']   = build_net('pool',net['conv3_4'])
  net['conv4_1'] = build_net('conv',net['pool3'],get_weight_bias(vgg_layers,19))
  net['conv4_2'] = build_net('conv',net['conv4_1'],get_weight_bias(vgg_layers,21))
  net['conv4_3'] = build_net('conv',net['conv4_2'],get_weight_bias(vgg_layers,23))
  net['conv4_4'] = build_net('conv',net['conv4_3'],get_weight_bias(vgg_layers,25))
  net['pool4']   = build_net('pool',net['conv4_4'])
  net['conv5_1'] = build_net('conv',net['pool4'],get_weight_bias(vgg_layers,28))
  net['conv5_2'] = build_net('conv',net['conv5_1'],get_weight_bias(vgg_layers,30))
  net['conv5_3'] = build_net('conv',net['conv5_2'],get_weight_bias(vgg_layers,32))
  net['conv5_4'] = build_net('conv',net['conv5_3'],get_weight_bias(vgg_layers,34))
  net['pool5']   = build_net('pool',net['conv5_4'])
  return net

def build_content_loss(p, x):
  loss = 0.5 * tf.reduce_sum(tf.pow((x - p),2))
  return loss


def gram_matrix(x, area, depth):
  x1 = tf.reshape(x,(area,depth))
  g = tf.matmul(tf.transpose(x1), x1)
  return g

def gram_matrix_val(x, area, depth):
  x1 = x.reshape(area,depth)
  g = np.dot(x1.T, x1)
  return g

def build_style_loss(a, x):
  M = a.shape[1]*a.shape[2]
  N = a.shape[3]
  A = gram_matrix_val(a, M, N )
  G = gram_matrix(x, M, N )
  loss = (1./(4 * N**2 * M**2)) * tf.reduce_sum(tf.pow((G - A),2))
  return loss



def read_image(path):
  image = scipy.misc.imread(path)
  #image = image[np.newaxis,-IMAGE_H:,:IMAGE_W,:] 
  image = image[np.newaxis,:IMAGE_H,:IMAGE_W,:] 
  image = image[:,:,:,::-1] 
  image = image - MEAN_VALUES
  return image

def write_image(path, image):
  image = image + MEAN_VALUES
  #image = image[:,::-1,:,::-1]
  image = image[:,:,:,::-1]
  image = image 
  image = image[0]
  image = np.clip(image, 0, 255).astype('uint8')
  scipy.misc.imsave(path, image)


def main():
  if not os.path.exists("./results"):
      os.mkdir("./results")
  net = build_vgg19('imagenet-vgg-verydeep-19.mat')
  sess = tf.Session()
  sess.run(tf.initialize_all_variables())
  noise_img = np.random.uniform(-20, 20, (1, IMAGE_H, IMAGE_W, 3)).astype('float32')
  content_img = read_image('./images/Tuebingen_small.jpg')
  style_img = read_image('./images/StarryNight_small.jpg')
  write_image("./results/content.png",content_img)
  write_image("./results/style.png",style_img)
  #content_layers =['conv4_2']
  #style_layers=['conv1_1','conv2_1','conv3_1','conv4_1','conv5_1']
  #failed at conv3_4
  _, r_content42 = sess.run([net['input'].assign(content_img), net['conv4_2']])
  cost_content = build_content_loss(r_content42 ,  net['conv4_2'])
  _, r_style11, r_style21, r_style31, r_style41, r_style51 = sess.run([net['input'].assign(style_img), 
    net['conv1_1'], net['conv2_1'], net['conv3_1'], net['conv4_1'], net['conv5_1']])
  cost_style =  build_style_loss(r_style11, net['conv1_1'])
  cost_style +=  build_style_loss(r_style21, net['conv2_1'])
  cost_style +=  build_style_loss(r_style31, net['conv3_1'])
  cost_style +=  build_style_loss(r_style41, net['conv4_1'])
  cost_style +=  build_style_loss(r_style51, net['conv5_1'])
  cost_total = cost_content + 5e2 * cost_style
  #cost_total = cost_content 
  #cost_total =  cost_style
  optimizer = tf.train.AdamOptimizer(1.0)
  ##optimizer = tf.train.MomentumOptimizer(0.02, -0.02)
  ##optimizer = tf.train.RMSPropOptimizer(2.0, 0.5)

  train = optimizer.minimize(cost_total)
  sess.run(tf.initialize_all_variables())
  sess.run(net['input'].assign( noise_img))



  for i in range(500):
    sess.run(train)
    #print result_cost
    if i%20 ==0:
      result_img = sess.run(net['input'])
      print sess.run(cost_total)
      write_image("./results/result_%s.png"%(str(i).zfill(4)),result_img)
      #print sess.run(net['conv1_1_weight'])
    
  

  

if __name__ == "__main__":
  main()
